# Solution For Task "Weather Data"

This repository contains the solution for the [task](TASK.md).

## Getting started
In order to get this repo running, first install *NPM*. Afterwards, follow these steps:
- Install dependencies via ``npm install``.
- Run the app by ``npm start``.
- Run the app in development mode by running ``npm run start:dev``.
- Run the tests with ``npm test``.

## Data Models and Structure
The following content roughly describes what was created handwritten beforehand starting with the development.

### Data Models 
_WeatherDataModel (extends BaseModel)_
- days: WeatherDataDayModel

_WeatherDataDayModel (extends BaseModel)_
- day: number
- minTemp: number
- maxTemp: number
- diffTemp: number

_FootballLeagueDataModel (extends BaseModel)_
- matches: FootballMatchDataModel

_FootballMatchDataModel_
- Team: string
- Goals: number
- Goals Allowed: number

### Structure
_App_
- runs the application.
- depends on the WeatherService.

_ModelMapperService_
- maps data from previous steps to fit the internally used data models.
- depends on parsed data from FileService.

_FileService_
- reads in files, returns the csv data in a format useable by further steps.
