import {FileService} from "./services/file.service";
import {catchError, finalize, map, mergeMap, tap} from "rxjs/operators";
import {FileContentType} from "./types/file-content.type";
import {ModelMapperService} from "./services/model-mapper.service";
import {WeatherDataInterface} from "./models/weather-data.model";
import {Observable} from "rxjs";
import {BaseModelInterface} from "./models/base.model";
import {FootballLeagueDataModelInterface} from "./models/football-league-data.model";

const chalk = require('chalk');

/**
 * A class that represents the application to run.
 *
 * @author Fabian Henkel <fabhenkel@gmail.com>
 */
export class App {

    /**
     * Internal function that fetches the file data, maps the data to the internlly used data format and puts everything out.
     *
     * @param name
     * @param fileName
     * @param mergeMapFn
     */
    private runChallenge<T extends BaseModelInterface>(name: string, fileName: string, mergeMapFn: (fileContent: FileContentType[]) => Observable<T>): Observable<T> {
        console.log(chalk.yellow(`======================`));
        console.log(chalk.yellow(`${name}`));
        console.log(chalk.yellow(`===`));

        // Read in the file, transform the observable to parse the file content and return the
        return FileService
            .readCSVFile(fileName)
            .pipe(
                mergeMap(mergeMapFn),
                catchError((error, caught) => {
                    console.log(chalk.red(`Error while reading the file!\n${error.message}`));
                    return caught;
                }),
                finalize(() => {
                    console.log(chalk.yellow(`======================`));
                })
            )
    }

    /**
     * Function that runs the weather challenge by reading out the csv file that was provided and printing out the
     * desired data to the console.
     *
     * @param fileName
     */
    public runWeatherChallenge(fileName: string): Observable<void> {
        return this.runChallenge<WeatherDataInterface>(
            'Weather Challenge',
            fileName,
            (fileContent: FileContentType[]) => ModelMapperService.mapFileContentToWeatherDataModel(fileContent))
            .pipe(map(
                (weatherDataModel: WeatherDataInterface) => {

                    // Find the day with the least temp distance and print it out!
                    const fittingDay = weatherDataModel.days.reduce((previousDay, currentDay) => {
                        return previousDay.diffTemp < currentDay.diffTemp ? previousDay : currentDay;
                    });
                    console.log(chalk.blue(`Fitting day: #${fittingDay.day} with a temperature spread of ${fittingDay.diffTemp}°C (between ${fittingDay.minTemp}°C and ${fittingDay.maxTemp}°C)`));
                }
            ));
    }

    /**
     * Function that runs the football challenge and logs the result from the provided data to the console.
     *
     * @param fileName
     */
    public runFootballChallenge(fileName: string): Observable<void> {
        return this.runChallenge<FootballLeagueDataModelInterface>(
            'Football Challenge',
            fileName,
            (fileContent: FileContentType[]) => ModelMapperService.mapFileContentToFootballDataModel(fileContent))
            .pipe(map(
                (footballLeagueDataModel: FootballLeagueDataModelInterface) => {

                    // Find the day with the least temp distance and print it out!
                    const fittingTeam = footballLeagueDataModel.matches.reduce((previousMatch, currentMatch) => {
                        return previousMatch.goalDistance < currentMatch.goalDistance ? previousMatch : currentMatch;
                    });
                    console.log(chalk.blue(`Team: #${fittingTeam.team} with a goal distance of ${fittingTeam.goalDistance} goals (self-scored: ${fittingTeam.goals} and scored against: ${fittingTeam.goalsAllowed})`));
                }
            ));
    }
}
