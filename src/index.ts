import {App} from "./app";
import {combineLatest, concat} from "rxjs";
import {mergeMap} from "rxjs/operators";

/**
 * The main file that runs the application to display values from the CSV file.
 *
 * @author Fabian Henkel <fabhenkel@gmail.com>
 */
const app = new App();
app
    .runWeatherChallenge('data/weather.csv')
    .pipe(mergeMap(() => app.runFootballChallenge('data/football.csv')))
    .subscribe();

