export interface BaseModelInterface {}

/**
 * A base model to extend from.
 *
 * @author Fabian Henkel <fabhenkel@gmail.com>
 */
export class BaseModel {

}
