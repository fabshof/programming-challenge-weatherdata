import {BaseModel, BaseModelInterface} from "./base.model";
import {FootballMatchInterface} from "./football-match-data.model";
import assert = require("assert");

export interface FootballLeagueDataModelInterface extends BaseModelInterface {
    matches: FootballMatchInterface[];
}

/**
 * A class that represents a whole league of football matches.
 *
 * @author Fabian Henkel <fabhenkel@gmail.com>
 */
export class FootballLeagueDataModel extends BaseModel implements FootballLeagueDataModelInterface {

    /**
     * Property that holds the matches of this league.
     */
    private _matches: FootballMatchInterface[] = [];
    get matches(): FootballMatchInterface[] {
        return this._matches;
    }
    set matches(value: FootballMatchInterface[]) {
        if (value) {
            assert(Array.isArray(value));
            this._matches = value;
        }
    }

    /**
     * Constructor with the optional property to set.
     *
     * @param matches
     */
    constructor(matches?: FootballMatchInterface[]) {
        super();
        this.matches = matches;
    }
}
