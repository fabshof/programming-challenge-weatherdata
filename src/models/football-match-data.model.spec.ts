import 'mocha';
import {expect} from 'chai';
import {WeatherDataDayModel} from "./weather-data-day.model";
import {FootballMatchDataModel} from "./football-match-data.model";

describe('FootballMatchDataModel', () => {
    it('#goals/ #goalAllowed should be properly set if valid.', () => {
        let matchModel = new FootballMatchDataModel();
        const goals = 1;
        const goalsAllowed = 5;

        matchModel.goals = goals;
        matchModel.goalsAllowed = goalsAllowed;

        expect(matchModel.goals).to.equal(goals);
        expect(matchModel.goalsAllowed).to.equal(goalsAllowed);
    });

    it('#goals/ #goalAllowed should throw errors if not properly set.', () => {
        let matchModel1 = new FootballMatchDataModel();
        let matchModel2 = new FootballMatchDataModel();
        const goals = -1;
        const goalsAllowed = -5;

        expect(() => {
            matchModel1.goals = goals;
        }).to.throw();

        expect(() => {
            matchModel2.goalsAllowed = goalsAllowed;
        }).to.throw();
    });

    it('#calculateGoalDistance should initially return -1.', () => {
        let matchModel = new FootballMatchDataModel();
        expect(matchModel.goalDistance).to.equal(-1);
    });

    it('#calculateGoalDistance should return the correct distance after setting only one value.', () => {
        let matchModel1 = new FootballMatchDataModel();
        let matchModel2 = new FootballMatchDataModel();
        const goals = 1;
        const goalsAllowed = 5;

        matchModel1.goals = goals;
        matchModel2.goalsAllowed = goalsAllowed;

        expect(matchModel1.goalDistance).to.equal(goals);
        expect(matchModel2.goalDistance).to.equal(goalsAllowed);
    });

    it('#calculateGoalDistance should return the correct distance.', () => {
        let matchModel1 = new FootballMatchDataModel();
        let matchModel2 = new FootballMatchDataModel();

        // Small and big numbers.
        matchModel1.goals = 1;
        matchModel1.goalsAllowed = 5;

        // Vice versa.
        matchModel2.goals = 5;
        matchModel2.goalsAllowed = 1;

        expect(matchModel1.goalDistance).to.equal(4);
        expect(matchModel2.goalDistance).to.equal(4);
    });
});
