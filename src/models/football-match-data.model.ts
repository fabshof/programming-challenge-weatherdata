import {BaseModel, BaseModelInterface} from "./base.model";
import assert = require("assert");

export interface FootballMatchInterface extends BaseModelInterface {
    team: string | undefined;
    goals: number | undefined;
    goalsAllowed: number | undefined;
    goalDistance: number | undefined;
}

/**
 * A single match is represented by this class.
 *
 * @author Fabian Henkel <fabhenkel@gmail.com>
 */
export class FootballMatchDataModel extends BaseModel implements FootballMatchInterface {

    /**
     * The name of the current team.
     */
    private _team: string;
    get team(): string {
        return this._team;
    }
    set team(value: string) {
        if (value) {
            assert(typeof value === 'string');
            this._team = value;
        }
    }

    /**
     * The number of goals that were made by the current team.
     */
    private _goals: number = 0;
    get goals(): number {
        return this._goals;
    }
    set goals(value: number) {
        if (value) {
            assert(typeof value === 'number');
            assert(value >= 0);
            this._goals = value;
            this._goalDistance = this.calculateGoalDistance(value, this.goalsAllowed);
        }
    }

    /**
     * The number of goals that were scored against this team.
     */
    private _goalsAllowed: number = 0;
    get goalsAllowed(): number {
        return this._goalsAllowed;
    }
    set goalsAllowed(value: number) {
        if (value) {
            assert(typeof value === 'number');
            assert(value >= 0);
            this._goalsAllowed = value;
            this._goalDistance = this.calculateGoalDistance(this.goals, value);
        }
    }

    /**
     * The absolute distance between self-scored goals and those which were scored against this team.
     */
    private _goalDistance: number = -1;
    get goalDistance(): number {
        return this._goalDistance;
    }

    /**
     * Constructor including the optional class properties to set.
     *
     * @param team
     * @param goals
     * @param goalsAllowed
     */
    constructor(team?: string, goals?: number, goalsAllowed?: number) {
        super();
        this.team = team;
        this.goals = goals;
        this.goalsAllowed = goalsAllowed;
    }

    /**
     * Calculate and return the absolute distance between the two goal counts.
     *
     * @param goalCountA
     * @param goalCountB
     */
    private calculateGoalDistance(goalCountA: number, goalCountB: number) {
        assert(typeof goalCountA === 'number');
        assert(typeof goalCountB === 'number');
        if (goalCountA >= 0 && goalCountB >= 0) {
            return Math.abs(goalCountA - goalCountB);
        }
        return -1;
    }

}
