import 'mocha';
import {expect} from 'chai';
import {WeatherDataDayModel} from "./weather-data-day.model";

describe('WeatherDataDayModel', () => {
    it('#minTemp/ #maxTemp should be properly set if valid.', () => {
        let dayModel = new WeatherDataDayModel();
        const minTemp = 1;
        const maxTemp = 5;

        dayModel.minTemp = minTemp;
        dayModel.maxTemp = maxTemp;

        expect(dayModel.minTemp).to.equal(minTemp);
        expect(dayModel.maxTemp).to.equal(maxTemp);
    });

    it('#minTemp/ #maxTemp should throw errors if not properly set.', () => {
        let dayModel1 = new WeatherDataDayModel();
        let dayModel2 = new WeatherDataDayModel();
        const minTemp = 5;
        const maxTemp = 1;

        dayModel1.minTemp = minTemp;
        dayModel2.maxTemp = maxTemp;

        expect(() => {
            dayModel1.maxTemp = maxTemp;
            dayModel2.minTemp = minTemp;
        }).to.throw();
    });

    it('#calculateDiffTemp should initially return -1.', () => {
        let dayModel = new WeatherDataDayModel();
        expect(dayModel.diffTemp).to.equal(-1);
    });
    it('#calculateDiffTemp should return -1 after providing only one of the temperatures.', () => {
        let dayModel1 = new WeatherDataDayModel();
        let dayModel2 = new WeatherDataDayModel();

        dayModel1.minTemp = 42;
        dayModel2.maxTemp = 42;

        expect(dayModel1.diffTemp).to.equal(-1);
        expect(dayModel2.diffTemp).to.equal(-1);
    });
    it('#calculateDiffTemp should return a correctly calculated difference.', () => {
        let dayModel1 = new WeatherDataDayModel();
        let dayModel2 = new WeatherDataDayModel();
        let dayModel3 = new WeatherDataDayModel();
        let dayModel4 = new WeatherDataDayModel();

        // Two negative values.
        dayModel1.minTemp = -5;
        dayModel1.maxTemp = -1;
        // A positive and a negative value.
        dayModel2.minTemp = -1;
        dayModel2.maxTemp = 5;
        // Two positive values.
        dayModel3.minTemp = 1;
        dayModel3.maxTemp = 5;

        expect(dayModel1.diffTemp).to.equal(4);
        expect(dayModel2.diffTemp).to.equal(6);
        expect(dayModel3.diffTemp).to.equal(4);
    });
});
