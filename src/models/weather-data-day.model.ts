import assert = require("assert");
import {BaseModel, BaseModelInterface} from "./base.model";

export interface WeatherDataDayInterface extends BaseModelInterface {
    day: number | undefined;
    maxTemp: number | undefined;
    minTemp: number | undefined;
    diffTemp: number;
}

/**
 * The models holds weather data for one day, including the day number, min- and max- temperatures.
 *
 * @author Fabian Henkel <fabhenkel@gmail.com>
 */
export class WeatherDataDayModel extends BaseModel implements WeatherDataDayInterface {

    /**
     * Property includes number of the current weather data day in a month.
     */
    private _day: number;
    get day(): number {
        return this._day;
    }

    set day(value: number) {
        if (value) {
            assert(typeof value === 'number')
            this._day = value;
        }
    }

    /**
     * A number property to hold the minimum temperature of a day.
     */
    private _minTemp: number;
    get minTemp(): number {
        return this._minTemp;
    }

    set minTemp(value: number) {
        if (value) {
            if (this.maxTemp) {
                assert(value <= this.maxTemp);
            }
            assert(typeof value === 'number');
            this._minTemp = value;
            this._diffTemp = this.calculateDiffTemp(value, this.maxTemp);
        }
    }

    /**
     * A number property to hold the maximum temperature of a day.
     */
    private _maxTemp: number;
    get maxTemp(): number {
        return this._maxTemp;
    }

    set maxTemp(value: number) {
        if (value) {
            if (this.minTemp) {
                assert(value >= this.minTemp);
            }
            assert(typeof value === 'number')
            this._maxTemp = value;
            this._diffTemp = this.calculateDiffTemp(this.minTemp, value);
        }
    }

    /**
     * Property that holds the absolute temperature difference between min and max.
     */
    private _diffTemp: number = -1;
    get diffTemp(): number {
        return this._diffTemp;
    }

    /**
     * Constructor that takes optional properties to be set if provided.
     *
     * @param day
     * @param minTemp
     * @param maxTemp
     */
    constructor(day?: number, minTemp?: number, maxTemp?: number) {
        super();
        this.day = day;
        this.minTemp = minTemp;
        this.maxTemp = maxTemp;
    }

    /**
     * Function to calculate the absolute difference between two provided temperatures.
     *
     * @param tempA
     * @param tempB
     */
    private calculateDiffTemp(tempA: number, tempB: number): number {
        if (tempA && tempB) {
            assert(typeof tempA === 'number');
            assert(typeof tempB === 'number');

            return Math.abs(tempA - tempB);
        }
        return -1;
    }
}
