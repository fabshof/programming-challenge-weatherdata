import assert = require("assert");
import {WeatherDataDayInterface} from "./weather-data-day.model";
import {BaseModel, BaseModelInterface} from "./base.model";

export interface WeatherDataInterface extends BaseModelInterface {
    days: WeatherDataDayInterface[];
}

/**
 * A model that holds a list of weather data day models and therefore is able to represent a group of days.
 *
 * @author Fabian Henkel <fabhenkel@gmail.com>
 */
export class WeatherDataModel extends BaseModel implements WeatherDataInterface {

    /**
     * The days property to store weather data models. Includes a private property, as well as a getter and setter.
     */
    private _days: WeatherDataDayInterface[] = [];
    public get days(): WeatherDataDayInterface[] {
        return this._days;
    }
    public set days(value: WeatherDataDayInterface[]) {
        if (value) {
            assert(Array.isArray(value));
            this._days = value;
        }
    }

    /**
     * Constructs a new weather data model with an optional days property.
     *
     * @param days
     */
    constructor(days?: WeatherDataDayInterface[]) {
        super();
        this.days = days;
    }
}
