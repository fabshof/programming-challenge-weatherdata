import {expect} from 'chai';
import 'mocha';
import {FileService} from "./file.service";
import {FileContentType} from "../types/file-content.type";

describe('FileService', () => {
    it('#readCSVFile should throw an error, if the file is not present.', (done) => {
        const fileName = 'example.csv';
        FileService.readCSVFile(fileName)
            .subscribe((data: FileContentType[]) => {
                expect(false).to.equal(true);
                done();
            }, (error) => {
                expect(error.message).to.include('example.csv does not exist!')
                done();
            });
    });

    it('#readCSVFile should throw an error, if the file is no CSV-file.', (done) => {
        const fileName = 'example.jpg';
        FileService.readCSVFile(fileName)
            .subscribe(() => {
                expect(false).to.equal(true);
                done();
            }, (error) => {
                expect(error.message).to.equal('Invalid file format!');
                done();
            });
    });

    it('#readCSVFile should successfully read the file.', (done) => {
        const fileName = 'data/weather.csv';
        FileService.readCSVFile(fileName)
            .subscribe((fileContent: FileContentType[]) => {
                expect(!!fileContent).to.equal(true);
                expect(Array.isArray(fileContent)).to.equal(true);
                expect(fileContent.length > 0).to.equal(true);
                done()
            }, () => {
                expect(false).to.equal(true);
                done();
            });
    });
});
