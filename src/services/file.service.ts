import {Observable} from "rxjs";
import {FileContentType} from "../types/file-content.type";
import {createReadStream, existsSync} from 'fs';
import {join} from 'path';

const csv = require('csv-parser');

/**
 * A service that reads in CSV files and returns an array of objects of the form:
 * {
 *     'header': 'value'
 * }
 *
 * @author Fabian Henkel <fabhenkel@gmail.com>
 */
export class FileService {

    private static validFileType: string = '.csv';

    /**
     * The function tries to read in a file, throws an exception if this is not possible. The filepath is considered
     * relative to the project rootIt returns an array of objects of the form { 'header': 'value', ... }, where 'header'
     * stands for the CSV column header and value represents the cell content formatted as a string.
     *
     * @param fileName
     */
    public static readCSVFile(fileName: string): Observable<FileContentType[]> {
        return new Observable<FileContentType[]>((subscriber) => {

            // Path relative to the project root.
            fileName = join(process.cwd(), fileName);

            // Check for the correct file type.
            if (!fileName.endsWith(this.validFileType)) {
                subscriber.error(new Error(`Invalid file format!`));
                subscriber.complete();
                return;
            }

            // Check if the file exists.
            if (!existsSync(fileName)) {
                subscriber.error(new Error(`File ${fileName} does not exist!`));
                subscriber.complete();
                return;
            }

            // Read the file content.
            const fileContent: FileContentType[] = [];
            createReadStream(fileName)
                .pipe(csv())
                .on('data', (rowData: FileContentType) => {
                    fileContent.push(rowData);
                })
                .on('end', () => {
                    subscriber.next(fileContent);
                    subscriber.complete();
                })
                .on('error', (error) => {
                    subscriber.error('Error while reading file!');
                    subscriber.complete();
                });
        });
    }
}
