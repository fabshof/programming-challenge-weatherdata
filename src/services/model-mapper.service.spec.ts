import {expect} from 'chai';
import 'mocha';
import {FileContentType} from "../types/file-content.type";
import {ModelMapperService} from "./model-mapper.service";
import {WeatherDataDayInterface} from "../models/weather-data-day.model";

describe('ModelMapperService', () => {
    it('#mapFileContentToWeatherDataModel should not map an empty weather file.', (done) => {
        const fileContent: FileContentType[] = [];
        ModelMapperService.mapFileContentToWeatherDataModel(fileContent)
            .subscribe((weatherDataModel) => {
                expect(weatherDataModel.days?.length === 0).to.equal(true);
                done();
            }, () => {
                expect(false).to.equal(true);
                done();
            });
    });
    it('#mapFileContentToWeatherDataModel should successfully deep map the weather data model.', (done) => {
        const fileContent: FileContentType[] = [
            {'Day': '1', 'MxT': '12', 'MnT': '1'},
        ];
        ModelMapperService.mapFileContentToWeatherDataModel(fileContent)
            .subscribe((weatherDataModel) => {
                expect(weatherDataModel.days?.length === 1).to.equal(true);
                const dayModel: WeatherDataDayInterface = weatherDataModel.days[0];
                expect(dayModel.day).to.equal(1);
                expect(dayModel.minTemp).to.equal(1);
                expect(dayModel.maxTemp).to.equal(12);
                done();
            }, () => {
                expect(false).to.equal(true);
                done();
            });
    });
});
