import {Observable, of} from "rxjs";
import {WeatherDataInterface, WeatherDataModel} from "../models/weather-data.model";
import {FileContentType} from "../types/file-content.type";
import {WeatherDataDayModel} from "../models/weather-data-day.model";
import {FootballColumnHeaders, WeatherColumnHeaders} from "../types/column-headers.type";
import {FootballLeagueDataModel, FootballLeagueDataModelInterface} from "../models/football-league-data.model";
import {FootballMatchDataModel} from "../models/football-match-data.model";

/**
 * A service that takes a parsed CSV-file and maps values to the internal data models.
 *
 * @author Fabian Henkel <fabhenkel@gmail.com>
 */
export class ModelMapperService {

    /**
     * A function that maps file content to the internally used weather data model.
     *
     * @param fileContent
     */
    public static mapFileContentToWeatherDataModel(fileContent: FileContentType[]): Observable<WeatherDataInterface> {

        const weatherDataModel = new WeatherDataModel();
        weatherDataModel.days = fileContent.map((fileRow: FileContentType) => {

            // Try to map string values to numbers and create a day model out of it.
            const day = Number(fileRow[WeatherColumnHeaders.DAY]);
            const minTemp = Number(fileRow[WeatherColumnHeaders.MIN_TEMP]);
            const maxTemp = Number(fileRow[WeatherColumnHeaders.MAX_TEMP]);

            const isDayValid = day && typeof day === 'number';
            const isMinTempValid = minTemp && typeof day === 'number';
            const isMaxTempValid = maxTemp && typeof day === 'number';

            return new WeatherDataDayModel(
                isDayValid ? day : undefined,
                isMinTempValid ? minTemp : undefined,
                isMaxTempValid ? maxTemp : undefined
            )
        });

        return of(weatherDataModel);
    }

    /**
     * Maps a football data model to the internal structure that represents a league with matches.
     *
     * @param fileContent
     */
    public static mapFileContentToFootballDataModel(fileContent: FileContentType[]): Observable<FootballLeagueDataModelInterface> {
        const footballLeagueDataModel = new FootballLeagueDataModel();
        footballLeagueDataModel.matches = fileContent.map((fileRow: FileContentType) => {

            // Validate the file data and create a match data model with it.
            const team = `${fileRow[FootballColumnHeaders.TEAM]}`;
            const goals = Number(fileRow[FootballColumnHeaders.GOALS]);
            const goalsAllowed = Number(fileRow[FootballColumnHeaders.GOALS_ALLOWED]);

            const isTeamValid = team && team !== '';
            const isGoalsValid = goals && typeof goals === 'number';
            const isGoalsAllowedValid = goalsAllowed && typeof goalsAllowed === 'number';

            return new FootballMatchDataModel(
                isTeamValid ? team : '',
                isGoalsValid ? goals : undefined,
                isGoalsAllowedValid ? goalsAllowed : undefined,
            )
        });

        return of(footballLeagueDataModel);
    }
}
