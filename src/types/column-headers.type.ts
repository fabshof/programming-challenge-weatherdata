/**
 * A type that represents the CSV columns of weather data.
 *
 * @author Fabian Henkel <fabhenkel@gmail.com>
 */
export enum WeatherColumnHeaders {
    DAY = 'Day',
    MIN_TEMP = 'MnT',
    MAX_TEMP = 'MxT',
}

/**
 * This type represents the CSV columns of football data.
 *
 * @author Fabian Henkel <fabhenkel@gmail.com>
 */
export enum FootballColumnHeaders {
    TEAM = 'Team',
    GOALS = 'Goals',
    GOALS_ALLOWED = 'Goals Allowed'
}
