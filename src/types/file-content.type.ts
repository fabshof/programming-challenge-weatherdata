/**
 * A type that represents the parsed content of a CSV file.
 *
 * @author Fabian Henkel <fabhenkel@gmail.com>
 */
export type FileContentType = {
    [key: string]: string;
}
